def vowel_swapper(string):
    # ==============
    # Your code here

    string1 = string.replace('a','4')
    string2 = string1.replace('A','4')
    string3 = string2.replace('e','3')
    string4 = string3.replace('E','3')
    string5 = string4.replace('i','!')
    string6 = string5.replace('I','!')
    string7 = string6.replace('o','ooo')
    string8 = string7.replace('O','000')
    string9 = string8.replace('u','|_|')
    string10 = string9.replace('U','|_|')

    return string10
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console