def factors(number):
    # ==============
    # Your code here
    list1 = []
    for i in range(1, number + 1):
        if number % i == 0:
            list1.append(i)

    list1.remove(1)
    list1.remove(number)

    return list1

    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
